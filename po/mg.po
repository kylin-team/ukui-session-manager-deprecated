# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Thierry Randrianiriana <randrianiriana@gmail.com>, 2006
msgid ""
msgstr ""
"Project-Id-Version: UKUI Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-12-29 16:47+0800\n"
"PO-Revision-Date: 2015-12-14 10:18+0000\n"
"Last-Translator: monsta <monsta@inbox.ru>\n"
"Language-Team: Malagasy (http://www.wiki.ukui.org/trans/ukui/UKUI/language/mg/)\n"
"Language: mg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../capplet/gsm-app-dialog.c:126
msgid "Select Command"
msgstr ""

#: ../capplet/gsm-app-dialog.c:198
msgid "Add Startup Program"
msgstr "Hampiditra rindranasam-piantombohana"

#: ../capplet/gsm-app-dialog.c:202
msgid "Edit Startup Program"
msgstr "Hanova rindranasam-piantombohana"

#: ../capplet/gsm-app-dialog.c:487
msgid "The startup command cannot be empty"
msgstr "Tsy azo atao foana ny baikom-piantombohana"

#: ../capplet/gsm-app-dialog.c:493
msgid "The startup command is not valid"
msgstr ""

#: ../capplet/gsm-properties-dialog.c:519
msgid "Enabled"
msgstr ""

#: ../capplet/gsm-properties-dialog.c:531
msgid "Icon"
msgstr ""

#: ../capplet/gsm-properties-dialog.c:543
msgid "Program"
msgstr "Rindranasa"

#: ../capplet/gsm-properties-dialog.c:742
msgid "Startup Applications Preferences"
msgstr ""

#: ../capplet/gsp-app.c:274
msgid "No name"
msgstr ""

#: ../capplet/gsp-app.c:280
msgid "No description"
msgstr ""

#: ../capplet/main.c:36 ../ukui-session/main.c:590
msgid "Version of this application"
msgstr ""

#: ../capplet/main.c:55
msgid "Could not display help document"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:1
msgid "Current session start time"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:2
msgid "Unix time of the start of the current session."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:3
msgid "Save sessions"
msgstr "Raiketo ny session"

#: ../data/org.ukui.session.gschema.xml.in.h:4
msgid "If enabled, ukui-session will save the session automatically."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:5
msgid "Logout prompt"
msgstr "Fanontaniana momba ny fivoahana"

#: ../data/org.ukui.session.gschema.xml.in.h:6
#, fuzzy
msgid "If enabled, ukui-session will prompt the user before ending a session."
msgstr ""
"Raha toa ka alefa, dia hanontany ny mpampiasa ny mate-session alohan'ny "
"hamaranana session iray."

#: ../data/org.ukui.session.gschema.xml.in.h:7
msgid "Logout timeout"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:8
msgid ""
"If logout prompt is enabled, this set the timeout in seconds before logout "
"automatically. If 0, automatic logout is disabled."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:9
msgid "Time before session is considered idle"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:10
msgid ""
"The number of minutes of inactivity before the session is considered idle."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:11
msgid "Default session"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:12
msgid "List of applications that are part of the default session."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:13
msgid "Required session components"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:14
msgid ""
"List of components that are required as part of the session. (Each element "
"names a key under \"/org/ukui/desktop/session/required_components\"). The "
"Startup Applications preferences tool will not normally allow users to "
"remove a required component from the session, and the session manager will "
"automatically add the required components back to the session at login time "
"if they do get removed."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:15
msgid "Control gnome compatibility component startup"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:16
msgid "Control which compatibility components to start."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:17
#: ../data/ukui-wm.desktop.in.in.h:1
msgid "Window Manager"
msgstr "Mpandrindra fikandrana"

#: ../data/org.ukui.session.gschema.xml.in.h:18
msgid ""
"The window manager is the program that draws the title bar and borders "
"around windows, and allows you to move and resize windows."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:19
msgid "Panel"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:20
msgid ""
"The panel provides the bar at the top or bottom of the screen containing "
"menus, the window list, status icons, the clock, etc."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:21
msgid "File Manager"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:22
msgid ""
"The file manager provides the desktop icons and allows you to interact with "
"your saved files."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:23
msgid "Dock"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:24
msgid ""
"A dock provides a dockable area, similar to a panel, for launching and "
"switching applications."
msgstr ""

#: ../data/ukui.desktop.in.h:1
msgid "UKUI"
msgstr ""

#: ../data/ukui.desktop.in.h:2
msgid "This session logs you into UKUI"
msgstr ""

#: ../data/gsm-inhibit-dialog.ui.h:1
msgid "<b>Some programs are still running:</b>"
msgstr ""

#: ../data/gsm-inhibit-dialog.ui.h:2 ../ukui-session/gsm-inhibit-dialog.c:635
msgid ""
"Waiting for the program to finish.  Interrupting the program may cause you "
"to lose work."
msgstr ""

#: ../data/ukui-session-properties.desktop.in.in.h:1
msgid "Startup Applications"
msgstr ""

#: ../data/ukui-session-properties.desktop.in.in.h:2
msgid "Choose what applications to start when you log in"
msgstr ""

#: ../data/session-properties.ui.h:1
msgid "Additional startup _programs:"
msgstr "_Rindranasam-piantombohana fanampiny:"

#: ../data/session-properties.ui.h:2
msgid "Startup Programs"
msgstr "Rindranasam-piatombohana"

#: ../data/session-properties.ui.h:3
msgid "_Automatically remember running applications when logging out"
msgstr ""

#: ../data/session-properties.ui.h:4
msgid "_Remember Currently Running Application"
msgstr ""

#: ../data/session-properties.ui.h:5
msgid "Options"
msgstr ""

#: ../data/session-properties.ui.h:6
msgid "Browse…"
msgstr ""

#: ../data/session-properties.ui.h:7
msgid "Comm_ent:"
msgstr ""

#: ../data/session-properties.ui.h:8
msgid "Co_mmand:"
msgstr ""

#: ../data/session-properties.ui.h:9
msgid "_Name:"
msgstr ""

#: ../egg/eggdesktopfile.c:153
#, c-format
msgid "File is not a valid .desktop file"
msgstr ""

#: ../egg/eggdesktopfile.c:173
#, c-format
msgid "Unrecognized desktop file Version '%s'"
msgstr ""

#: ../egg/eggdesktopfile.c:957
#, c-format
msgid "Starting %s"
msgstr ""

#: ../egg/eggdesktopfile.c:1098
#, c-format
msgid "Application does not accept documents on command line"
msgstr ""

#: ../egg/eggdesktopfile.c:1166
#, c-format
msgid "Unrecognized launch option: %d"
msgstr ""

#: ../egg/eggdesktopfile.c:1365
#, c-format
msgid "Can't pass document URIs to a 'Type=Link' desktop entry"
msgstr ""

#: ../egg/eggdesktopfile.c:1386
#, c-format
msgid "Not a launchable item"
msgstr ""

#: ../egg/eggsmclient.c:226
msgid "Disable connection to session manager"
msgstr ""

#: ../egg/eggsmclient.c:229
msgid "Specify file containing saved configuration"
msgstr ""

#: ../egg/eggsmclient.c:229
msgid "FILE"
msgstr ""

#: ../egg/eggsmclient.c:232
msgid "Specify session management ID"
msgstr ""

#: ../egg/eggsmclient.c:232
msgid "ID"
msgstr ""

#: ../egg/eggsmclient.c:253
msgid "Session management options:"
msgstr ""

#: ../egg/eggsmclient.c:254
msgid "Show session management options"
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:253
#, c-format
msgid "Icon '%s' not found"
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:583
msgid "Unknown"
msgstr "Tsy fantatra"

#: ../ukui-session/gsm-inhibit-dialog.c:634
msgid "A program is still running:"
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:638
msgid "Some programs are still running:"
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:639
msgid ""
"Waiting for programs to finish.  Interrupting these programs may cause you "
"to lose work."
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:869
msgid "Switch User Anyway"
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:872
msgid "Log Out Anyway"
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:875
msgid "Suspend Anyway"
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:878
msgid "Hibernate Anyway"
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:881
msgid "Shut Down Anyway"
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:884
msgid "Reboot Anyway"
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:892
msgid "Lock Screen"
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:895
msgid "Cancel"
msgstr ""

#: ../ukui-session/gsm-logout-dialog.c:333
#, c-format
msgid "You will be automatically logged out in %d second"
msgid_plural "You will be automatically logged out in %d seconds"
msgstr[0] ""
msgstr[1] ""

#: ../ukui-session/gsm-logout-dialog.c:341
#, c-format
msgid "This system will be automatically shut down in %d second"
msgid_plural "This system will be automatically shut down in %d seconds"
msgstr[0] ""
msgstr[1] ""

#: ../ukui-session/gsm-logout-dialog.c:387
#, c-format
msgid "You are currently logged in as \"%s\"."
msgstr ""

#: ../ukui-session/gsm-logout-dialog.c:467
msgid "Log out of this system now?"
msgstr ""

#: ../ukui-session/gsm-logout-dialog.c:473
msgid "_Switch User"
msgstr ""

#: ../ukui-session/gsm-logout-dialog.c:482
msgid "_Log Out"
msgstr ""

#: ../ukui-session/gsm-logout-dialog.c:488
msgid "Shut down this system now?"
msgstr ""

#: ../ukui-session/gsm-logout-dialog.c:494
msgid "S_uspend"
msgstr ""

#: ../ukui-session/gsm-logout-dialog.c:500
msgid "_Hibernate"
msgstr ""

#: ../ukui-session/gsm-logout-dialog.c:506
msgid "_Restart"
msgstr ""

#: ../ukui-session/gsm-logout-dialog.c:516
msgid "_Shut Down"
msgstr ""

#: ../ukui-session/gsm-manager.c:1436 ../ukui-session/gsm-manager.c:2155
msgid "Not responding"
msgstr ""

#. It'd be really surprising to reach this code: if we're here,
#. * then the XSMP client already has set several XSMP
#. * properties. But it could still be that SmProgram is not set.
#.
#: ../ukui-session/gsm-xsmp-client.c:566
msgid "Remembered Application"
msgstr ""

#: ../ukui-session/gsm-xsmp-client.c:1204
msgid "This program is blocking logout."
msgstr ""

#: ../ukui-session/gsm-xsmp-server.c:326
msgid ""
"Refusing new client connection because the session is currently being shut "
"down\n"
msgstr ""

#: ../ukui-session/gsm-xsmp-server.c:588
#, c-format
msgid "Could not create ICE listening socket: %s"
msgstr ""

#. Oh well, no X for you!
#: ../ukui-session/gsm-util.c:356
#, c-format
msgid "Unable to start login session (and unable to connect to the X server)"
msgstr ""

#: ../ukui-session/main.c:587
msgid "Override standard autostart directories"
msgstr ""

#: ../ukui-session/main.c:588
msgid "Enable debugging code"
msgstr ""

#: ../ukui-session/main.c:589
msgid "Do not load user-specified applications"
msgstr ""

#: ../ukui-session/main.c:610
#, fuzzy
msgid " - the UKUI session manager"
msgstr "Tsy afaka nifandray tamin'ilay mpandrindra session"

#: ../tools/ukui-session-save.c:70
msgid "Log out"
msgstr ""

#: ../tools/ukui-session-save.c:71
msgid "Log out, ignoring any existing inhibitors"
msgstr ""

#: ../tools/ukui-session-save.c:72
msgid "Show logout dialog"
msgstr ""

#: ../tools/ukui-session-save.c:73
msgid "Show shutdown dialog"
msgstr ""

#: ../tools/ukui-session-save.c:74
msgid "Use dialog boxes for errors"
msgstr ""

#. deprecated options
#: ../tools/ukui-session-save.c:76
msgid "Set the current session name"
msgstr "Hamaritra ny anaran'ity session ity"

#: ../tools/ukui-session-save.c:76
msgid "NAME"
msgstr "ANARANA"

#: ../tools/ukui-session-save.c:77
msgid "Kill session"
msgstr "Vonoy io session io"

#: ../tools/ukui-session-save.c:78
msgid "Do not require confirmation"
msgstr ""

#. Okay, it wasn't the upgrade case, so now we can give up.
#: ../tools/ukui-session-save.c:136 ../tools/ukui-session-save.c:151
msgid "Could not connect to the session manager"
msgstr "Tsy afaka nifandray tamin'ilay mpandrindra session"

#: ../tools/ukui-session-save.c:277
msgid "Program called with conflicting options"
msgstr ""

#: ../tools/ukui-session-inhibit.c:116
#, c-format
msgid ""
"%s [OPTION...] COMMAND\n"
"\n"
"Execute COMMAND while inhibiting some session functionality.\n"
"\n"
"  -h, --help        Show this help\n"
"  --version         Show program version\n"
"  --app-id ID       The application id to use\n"
"                    when inhibiting (optional)\n"
"  --reason REASON   The reason for inhibiting (optional)\n"
"  --inhibit ARG     Things to inhibit, colon-separated list of:\n"
"                    logout, switch-user, suspend, idle, automount\n"
"\n"
"If no --inhibit option is specified, idle is assumed.\n"
msgstr ""

#: ../tools/ukui-session-inhibit.c:170 ../tools/ukui-session-inhibit.c:180
#: ../tools/ukui-session-inhibit.c:190
#, c-format
msgid "%s requires an argument\n"
msgstr ""

#: ../tools/ukui-session-inhibit.c:226
#, c-format
msgid "Failed to execute %s\n"
msgstr ""
